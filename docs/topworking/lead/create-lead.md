---
sidebar_position: 1
---

# Tạo lead

## Đường dẫn gốc
[/khach-hang/tao-moi](http://dktt.topcv.local/khach-hang/tao-moi)

## Mô tả luồng

- Đường dẫn trên mapping với controller: LeadsController@addLead
- Controller trả về giao diện: v2.leads.new-lead
- Giao diện có chứa form, khi submit gửi lên đường dẫn: [/lead/ajax-add-new-lead](http://dktt.topcv.local/lead/ajax-add-new-lead) (Gửi ajax, hàm addLead trong Vue instance.)
- Đường dẫn trên mapping với controller: LeadsController@ajaxAddNewLead

## Hình ảnh mô tả
![Create lead](./resources/check-mail-create-lead.png "Create lead")

![Create lead](./resources/create-lead.png "Create lead")

## Mô tả cách sử dụng

- Ở màn hình 1: kiểm tra email xem đã tồn tại trên hệ thống chưa. Nếu không => Chuyển sang màn hình 2.
- Ở màn hình 2: Nhập thông tin của khách hàng. Các trường bắt buộc nhập bao gồm:
    + Họ tên, nguồn, SDT, tỉnh/thành phố, trạng thái khách hàng.
    + SDT có thể nhập nhiều.
- Sau khi đã nhập đủ thông tin, ấn submit sẽ nhận được thông báo thành công, có thể lựa chọn tiếp tục tạo mới hoặc về trang danh sách nếu từ chối.

## Mô tả logics

- Logic tạo lead chính nằm trong hàm LeadsController@ajaxAddNewLead.

- B1: Validate dữ liệu đầu vào theo yêu cầu.
- B2: Tạo lead từ data trên (lưu vào bảng leads)
- B3: Nếu người tạo lead không thuộc về đội cv scout, hoặc đội cộng tác viên:
    + Lead sẽ được assign cho admin tạo để care.
    + Tạo `lead_actions`, có type = change_take_care (Log lại lead đã được assign vào thời điểm tạo)
    + Tạo `care_period_records` (Log lại lead đã được care bởi salesman vào thời điểm tạo)
- B4: Nếu lựa chọn tạo tài khoản:
    + Tạo `employers` mới, là tài khoản cho nhà tuyển dụng, với password random
    + Gửi email cho NTD với nội dung tài khoản đã được tạo.